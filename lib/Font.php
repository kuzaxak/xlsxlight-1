<?php
namespace XLSXLight;

use Exception;

class Font
{
    private $tag = null;
    private $name = 'Calibri';
    private $size = 11;
    private $color = null;
    private $bold = false;
    private $italic = false;
    private $underline = false;
    private $charset = 186;
    private $family = 2;
    private $scheme = 'minor';
    private $strikeThrow = false;
    private $alignment = null;

    function __construct($fontTag)
    {
        $this->tag = $fontTag;
    }

    public function setName($fontName)
    {
        $this->name = $fontName;
        return $this;
    }

    //here goes number
    public function setFamily($family)
    {
        $this->family = $family;
        return $this;
    }

    public function setSize($number)
    {
        $this->size = $number;
        return $this;
    }

    public function setBold(){
        $this->bold = true;
        return $this;
    }

    public function setItalic(){
        $this->italic = true;
        return $this;
    }

    public function setUndeline(){
        $this->underline = true;
        return $this;
    }

    public function setStrikeThrow(){
        $this->strikeThrow = true;
        return $this;
    }

    public function setColor($rgbColor)
    {
        $this->color = $rgbColor;
        return $this;
    }

    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     * first horizontal [left|center|right] then vertical [top|center|bottom]
     * ex: left bottom
     * @param string $alignment
     * @return $this
     * @throws Exception
     */
    public function setAlignment($alignment)
    {
        if (!preg_match('/^(left|center|right)[ ]?(top|center|bottom)?$/', $alignment, $matches)) {
            throw new Exception('Incorrect alignment parameter ' . $alignment);
        }
        $this->alignment['horizontal'] = $matches[1];
        if (isset($matches[2])) {
            $this->alignment['vertical'] = $matches[2];
        }
        return $this;
    }

    public function getAlignment()
    {
        return $this->alignment;
    }

    public function getTag(){
        return $this->tag;
    }

    public function generateStyle(){
        $xml = '<font>';
        if($this->bold){
            $xml .= '<b/>';
        }
        if($this->underline){
            $xml .= '<u val="'.strval($this->underline).'"/>';
        }
        if($this->italic){
            $xml.='<i/>';
        }
        if($this->strikeThrow){
            $xml.='<strike/>';
        }
        $xml .= '<sz val="'.$this->size.'"/><name val="'.$this->name.'"/><family val="'.$this->family.'"/><charset val="'.$this->charset.'"/><scheme val="'.$this->scheme.'"/>';
        if($this->color){
            $xml.='<color rgb="'.$this->color.'"/>';
        }
        $xml.='</font>';
        return $xml;
    }
}