<?php
namespace XLSXLight;

/**
 * Class XLSXLightRow
 *
 * @property ObjectIterator $cells
 */
class Row
{
    private $cells;

    public function __construct()
    {
        $this->cells = new ObjectIterator();
    }

    public function addCell(Cell $cell)
    {
        $this->cells->addItem($cell, $cell->getIndex());
        return $this;
    }

    public function getCells()
    {
        $this->cells->sort();
        return $this->cells;
    }

    public function hasCell($cellIndex)
    {
        return $this->cells->hasItem($cellIndex);
    }
}