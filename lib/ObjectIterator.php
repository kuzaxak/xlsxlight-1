<?php

namespace XLSXLight;

use Iterator;

class ObjectIterator implements Iterator
{
    private $items = [];

    public function current()
    {
        return current($this->items);
    }

    public function next()
    {
        next($this->items);
    }

    public function flush()
    {
        $this->items = [];
        return $this;
    }

    public function remove($key)
    {
        unset($this->items[$key]);
    }

    public function key()
    {
        return key($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function valid()
    {
        return key($this->items) !== null;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function sort()
    {
        ksort($this->items);
        return $this;
    }

    public function hasItem($key)
    {
        return isset($this->items[$key]);
    }

    /**
     * @param $key
     * @return Cell|Row|Sheet|bool
     */
    public function getItem($key)
    {
        return $this->hasItem($key) ? $this->items[$key] : false;
    }

    /**
     * @param mixed $item
     * @param null|string|int $key
     * @return $this
     */
    public function addItem($item, $key = null)
    {
        if ($key) {
            $this->items[$key] = $item;
        } else {
            $this->items[] = $item;
        }
        return $this;
    }
}
