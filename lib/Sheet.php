<?php
namespace XLSXLight;

use DateTime;
use Exception;

class Sheet
{
    private $title;
    private $WORKBOOK;
    private $id;
    private $defaultRowHeight = 20;
    private $defaultColWidth = 10;
    private $tempFile;
    private $directWrite = true;
    private $fs;
    private $lockOut;
    private $data;
    private $images = [];
    private $columnWidth = [];
    private $mergedCells = [];
    private $paneFreez;

    function __construct($title, Workbook $workbook, $directWrite = true)
    {
        $this->id = ($workbook->sheets->count() + 1);
        $workbook->sheets->addItem($this);
        $this->WORKBOOK = $workbook;
        $this->data = new ObjectIterator();
        $this->title = $title;
        $this->directWrite = $directWrite;
        $this->tempFile = tempnam('tmp/', 'xlsxlight_sheet');
        $this->fs = fopen($this->tempFile, 'w');
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function freezePanes($cellIndex)
    {
        $this->paneFreez = $cellIndex;
        return $this;
    }

    /**
     * @param int $height in pixels
     * @return $this
     */
    public function setDefaultRowHeight($height)
    {
        //Maximum row height in Excel is 409px
        $this->defaultRowHeight = ($height > 409) ? 409 : $height;
        return $this;
    }

    /**
     * @param int $width in pixels
     * @return $this
     */
    public function setDefaultColWidth($width)
    {
        //excel stores column with with the ratio 1/6 in pixels
        $this->defaultColWidth = $width / Workbook::COLUMN_UNIT_RATIO;
        return $this;
    }

    public function getDefaultRowHeight()
    {
        return $this->defaultRowHeight;
    }

    public function getDefaultColWidth()
    {
        return $this->defaultColWidth;
    }

    public function setColumnWidth($column, $width)
    {
        $columns = explode(':', $column);
        $min = (Workbook::getColumnNumber($columns[0]) + 1);
        $count = (isset($columns[1])) ? (Workbook::getColumnNumber($columns[1]) + 1) - $min + 1 : 1;
        $range = array_fill($min, $count, round($width / Workbook::COLUMN_UNIT_RATIO, 7));
        $this->columnWidth = array_replace($this->columnWidth, $range);
        return $this;
    }

    public function getColumnWidth($columnNumber)
    {
        return (isset($this->columnWidth[$columnNumber]))
            ? $this->columnWidth[$columnNumber] * Workbook::COLUMN_UNIT_RATIO
            : $this->defaultColWidth * Workbook::COLUMN_UNIT_RATIO;
    }

    public function getRangeWidth($column)
    {
        $columns = explode(':', $column);
        $min = (Workbook::getColumnNumber($columns[0]) + 1);
        $max = (isset($columns[1])) ? (Workbook::getColumnNumber($columns[1]) + 1) : $min;
        $width = 0;
        foreach (range($min, $max) as $number) {
            $width += $this->getColumnWidth($number);
        }
        return $width;
    }

    private function writeSheetHead()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">'
            . '<dimension ref="A1:AA100"/>'
            . '<sheetViews>';
        $xml .= '<sheetView' . ($this->WORKBOOK->getActiveSheet() == $this->getId() ? ' tabSelected="1"' : '') . ' workbookViewId="0"';
        if ($this->paneFreez) {
            $xml .= '>';
            $frozenCellPosition = Workbook::getCellPosition($this->paneFreez);
            $xml .= '<pane xSplit="' . $frozenCellPosition['col'] . '" ySplit="' . ($frozenCellPosition['row'] - 1) . '" topLeftCell="' . $this->paneFreez . '" activePane="bottomRight" state="frozen"/>';
            //$xml .= '<selection pane="topRight" activeCell="B1" sqref="B1"/><selection pane="bottomLeft" activeCell="A6" sqref="A6"/><selection pane="bottomRight" activeCell="B6" sqref="B6"/>';
            $xml .= '</sheetView>';
        } else {
            $xml .= '/>';
        }

        $xml .= '</sheetViews>'
            . '<sheetFormatPr baseColWidth="0" defaultColWidth="' . $this->getDefaultColWidth() . '" defaultRowHeight="' . $this->getDefaultRowHeight() . '" customHeight="1" x14ac:dyDescent="0.4"/>';
        $xml .= $this->getCustomColumns();
        $xml .= '<sheetData>';
        fwrite($this->fs, $xml);
    }

    private function getCustomColumns()
    {
        $xml = '';
        if (!empty($customColumnWidth = $this->columnWidth)) {
            $xml .= '<cols>';
            $minNumber = false;
            $maxNumber = false;
            $lastColumnWidth = false;
            $lastColumnNumber = end(array_keys($customColumnWidth));
            foreach ($customColumnWidth as $columnNumber => $columnWidth) {
                if (!$minNumber) {
                    $minNumber = $columnNumber;
                }
                if ($lastColumnWidth !== false && ($columnWidth != $lastColumnWidth || $maxNumber + 1 != $columnNumber)) {
                    $xml .= '<col min="' . ($minNumber ?: $columnNumber) . '" max="' . ($maxNumber ?: $columnNumber) . '" width="' . $lastColumnWidth . '" customWidth="1"/>';
                    $minNumber = $columnNumber;
                }
                $lastColumnWidth = $columnWidth;
                $maxNumber = $columnNumber;
                if ($columnNumber == $lastColumnNumber) {
                    $xml .= '<col min="' . ($minNumber ?: $columnNumber) . '" max="' . ($maxNumber ?: $columnNumber) . '" width="' . $lastColumnWidth . '" customWidth="1"/>';
                }

            }
            $xml .= '</cols>';
        }
        return $xml;
    }

    private function generateData()
    {
        if (!$this->data->valid()) {
            return false;
        }
        $this->data->sort();
        while ($this->data->valid()) {
            $key = $this->data->key();
            $row = $this->data->current();
            if (empty($row->getCells()->count())) {
                continue;
            }
            $xml = '<row r="' . $key . '" x14ac:dyDescent="0.4">';
            /** @var Cell $cell */
            foreach ($row->getCells() as $cell) {

                $styleId = $this->WORKBOOK->getStyle(
                    $cell->getFont(),
                    $cell->getFormat(),
                    $cell->getFill(),
                    $cell->getBorder()
                );

                $value = ($cell->getValue() instanceof DateTime)
                    ? (25569 + $cell->getValue()->getTimestamp() / 86400)
                    : $cell->getValue();
                if ($value === null) {
                    $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" />';
                } elseif ($cell->getFormat()) {
                    $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="n"><v>' . $value . '</v></c>';
                } elseif ($value[0] === '=' && is_string($value)) {
                    $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="s"><f>' . ltrim($value, '=') . '</f></c>';
                } elseif (is_string($value)) {
                    $xml .= '<c r="' . $cell->getIndex() . '" s="' . $styleId . '" t="s"><v>' . $this->WORKBOOK->addSharedString($value) . '</v></c>';
                }
            }
            $xml .= '</row>';
            if (!$this->lockOut) {
                //Write sheet file heading before any data kets written to file.
                $this->writeSheetHead();
            }
            $this->lockOut = $key;
            $this->data->next();
            $this->data->remove($key);
            fwrite($this->fs, $xml);
        }
        return true;
    }

    public function getSheetFile()
    {
        $this->generateData();

        $xml = '</sheetData>';
        if (!empty($this->mergedCells)) {
            $xml .= '<mergeCells count="' . count($this->mergedCells) . '">';
            foreach ($this->mergedCells as $mergedCell) {
                $xml .= '<mergeCell ref="' . $mergedCell . '"/>';
            }
            $xml .= '</mergeCells>';
        }
        $xml .= '<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>';
        if (!empty($this->images)) {
            $xml .= '<drawing r:id="rId1" />';
        }
        $xml .= '</worksheet>';
        fwrite($this->fs, $xml);
        return $this->tempFile;
    }

    /**
     * @param Cell $cell
     * @return $this
     * @throws Exception
     */
    public function setCell(Cell $cell)
    {
        if ($this->lockOut >= $cell->getRow()) {
            throw new Exception('Unable to set data for Cell ' . $cell->getIndex() . ', row is all ready locked!');
        } elseif (!$this->data->hasItem($cell->getRow())) {
            if ($this->data->count() > 0 && $this->directWrite) {
                $this->generateData();
            }
            $this->data->addItem(new Row(), $cell->getRow());
        }
        $existingRow = $this->data->getItem($cell->getRow());
        if (!$existingRow) {
            throw new Exception('Row not found' . $cell->getIndex());
        }
        $existingRow->addCell($cell);
        if ($cell->getMergeRange()) {
            $this->mergedCells[] = $cell->getMergeRange();
            foreach ($cell->getMergedRows() as $row) {
                foreach ($cell->getMergedColumns() as $col) {
                    if ($this->data->getItem($row) && $this->data->getItem($row)->hasCell(Workbook::getColumnLetter($col) . $row)) {
                        continue;
                    }
                    $this->setCell((new Cell(Workbook::getColumnLetter($col) . $row))
                        ->setBorder($cell->getBorder())
                        ->setFill($cell->getFill())
                    );
                }
            }
        }
        return $this;
    }

    public function mergeCells($range)
    {
        if (is_array($range)) {
            foreach ($range as $r) {
                $this->mergeCells($r);
            }
        }
        preg_match("/^(([A-Z]+)([0-9]+))[:]?(([A-Z]+)([0-9]+))?$/", $range, $match);
        if (sizeof($match) != 7) {
            throw new Exception('Invalid Cell range "' . $range . '"');
        }
        $this->mergedCells[] = $range;

        return $this;
    }

    public function addImage(Image $image)
    {
        $image->setId(count($this->images) + 1);
        $this->images[] = $image;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function adjustImagePositions()
    {
        /** @var Image $image */
        foreach ($this->images as $image) {
            $offset = $image->getOffset();

            $imageWidth = $image->getWidth();
            $col = $image->getColIndex() + 1;
            $remaining = $colLeftOver = $imageWidth;
            while ($remaining >= 0) {
                $remaining -= $this->getColumnWidth($col);
                if ($remaining >= 0) {
                    $colLeftOver = $remaining;
                    $col++;
                }
            };
            $image->setToColIndex($col - 1);
            $imageHeight = $image->getHeight();
            $rowAmount = intval(($imageHeight + $offset['top']) / $this->defaultRowHeight);
            $rowLeftOver = ($imageHeight + $offset['top']) - ($rowAmount * $this->defaultRowHeight);
            $image->setToRowIndex($image->getRowIndex() + $rowAmount);

            $image->setOffset($offset['left'], $offset['top'], $colLeftOver, $rowLeftOver);
        }
    }
}